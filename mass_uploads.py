import os
import time
import sys


ip = "127.0.0.1"
port = "8484"
if len(sys.argv) == 4:
   f = sys.argv[1]
   ip = sys.argv[2]
   port = sys.argv[3]
elif len(sys.argv) == 3:
   f = sys.argv[1]
   port = sys.argv[2]
elif len(sys.argv) == 2:
   f = sys.argv[1]
else:
  exit()

print "curl -F file=@%s http://%s:%s/" % (f, ip, port)

start = time.time()
for i in xrange(1000):
    cmd = "curl -F file=@%s http://%s:%s/" % (f, ip, port)
    os.system(cmd)
    print
stop = time.time()

print stop - start
